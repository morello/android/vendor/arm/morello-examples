/*
 * Copyright (c) 2022 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * **************************************************************************
 * This is for demonstration purposes only, shall not be used in production.
 * **************************************************************************
 */
#include <cheriintrin.h>

struct simple_string_t *new_unmodifiable_string(intptr_t str_addr) {
    return cheri_perms_and(new_string(str_addr), ~CHERI_PERM_STORE);
}

int main() {
    intptr_t str_ptr = (intptr_t)str;
    struct simple_string_t *string = new_unmodifiable_string(str_ptr);

    printf("[string] \"%s\"\n", string->buffer);
    string->buffer[0] = 'P'; // Error
}
