/*
 * Copyright (c) 2022 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * **************************************************************************
 * This is for demonstration purposes only, shall not be used in production.
 * **************************************************************************
 */
#include <sanitizer/cheriseed_interface.h>

int main() {
    intptr_t str_ptr = (intptr_t)str;
    __cheriseed_control_checks(CHERISEED_DISABLE, CHERISEED_CHECK_TAG);
    struct simple_string_t *string = new_string(str_ptr);
    __cheriseed_control_semantics(CHERISEED_DISABLE);
    printf("[string] \"%s\"\n", string->buffer);
    free(string);
}
