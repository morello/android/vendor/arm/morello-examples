/*
 * Copyright (c) 2022 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * **************************************************************************
 * This is for demonstration purposes only, shall not be used in production.
 * **************************************************************************
 *
 * This is an example implementation of Feistel cipher algorithm.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

// Types of cipher operations.
typedef enum {
    ENCODE,
    DECODE,
} Operation;

// Definition of cipher data.
typedef union {
    uintptr_t addr;
    struct {
        void *left;
        void *right;
    } internal;
} Data;

// Definition of cipher block.
typedef struct {
    Data data;
    void *key;
    int data_len, key_len;
} Block;

// Maximum iterations defined for this particular Feistel program.
const int max_iteration = 4;

// Performs the XOR'ed string from 2 input strings and copies to the first arg.
void xor(uintptr_t addr_a, uintptr_t addr_b, int len);
// Performs the round function of a string and a character.
void round_func(uintptr_t addr, int len, uint8_t v);


// Performs recursive iteration of Feistel's algorithm on a Block for a specific
// iteration. Dangling characters are ignored while encoding/decoding. Last pass
// is just to swap left and right string.
void *feistel(Block *b, Operation op, int iteration) {
    // End condition.
    if (iteration == -1) return (void *)b->data.addr;
    // Length of half string.
    int pivot = (b->data_len) / 2;
    // Creates a copy of the old block.
    Data old_data = b->data;
    b->data.internal.left = calloc(b->data_len + 1, sizeof(char));
    b->data.internal.right = (void *)b->data.addr + pivot;
    memcpy((void *)b->data.addr, (void *)old_data.addr, b->data_len);
    // Left_i+1 = Right_i
    memcpy(b->data.internal.left, old_data.internal.right, pivot);
    // Computes Right_i+1
    if (iteration > 0) {
        int sub_key_idx = (b->key_len < max_iteration) ? b->key_len : max_iteration;
        sub_key_idx = ((op == DECODE) ? (max_iteration - iteration) : (iteration - 1)) % sub_key_idx;
        round_func((uintptr_t)old_data.internal.right, pivot, ((uint8_t *)b->key)[sub_key_idx]);

        xor((uintptr_t)old_data.internal.left, (uintptr_t)old_data.internal.right, pivot);
    }
    // Right_i+1 = Left_i
    memcpy(b->data.internal.right, old_data.internal.left, pivot);
    // Frees the old block.
    free((void *)old_data.addr);
    return feistel(b, op, --iteration);
}

void TEST(char *_text, char *_key);
// Main function running local tests.
int main() {
    TEST(/* text */"THIS"   /* key */, "THIS");
    TEST(/* text */"IS"     /* key */, "KEY");
    TEST(/* text */"SECRET" /* key */, "IS");
    TEST(/* text */"MESSAGE"/* key */, "GOOD");

    return 0;
}

void xor(uintptr_t addr_a, uintptr_t addr_b, int len) {
    for (char *a_chr = (char *)addr_a, *b_chr = (char *)addr_b;
         a_chr < (char *)(addr_a + sizeof(char) * len);
         a_chr+=sizeof(char), b_chr+=sizeof(char))
        *a_chr = *a_chr ^ *b_chr;
}

void round_func(uintptr_t addr, int len, uint8_t v) {
    for (char *chr = (char *)addr;
         chr < (char *)(addr + sizeof(char) * len);
         chr+=sizeof(char))
        *chr = (*chr + v);
}

// Function to call Feistel algorithm.
void *run_feistel(char *text, char *key, Operation op) {
    int text_length = strlen(text);
    int key_length = strlen(key);

    void *text_ptr = calloc(text_length + 1, sizeof(char));
    memcpy(text_ptr, text, sizeof(char) * text_length);
    void *key_ptr = calloc(key_length + 1, sizeof(char));
    memcpy(key_ptr, key, sizeof(char) * key_length);

    Block *block = calloc(1, sizeof(Block));
    block->data = (Data){ .internal = { text_ptr, text_ptr + (text_length/2) }};
    block->data_len = text_length;
    block->key = key_ptr;
    block->key_len = key_length;

    void *result = feistel(block, op, max_iteration);
    free(key_ptr);
    return result;
}

static int test_count = 0;
void TEST(char *_text, char *_key) {
    test_count++;
    void *_encoded_text = run_feistel(_text, _key, ENCODE);
    void *_decoded_text = run_feistel(_encoded_text, _key, DECODE);
    printf("TEST %2d: %s\n",
        test_count,
        !memcmp(_text, _decoded_text, strlen(_text)) ? "PASSED": "FAILED");
    free(_encoded_text);
    free(_decoded_text);
}
