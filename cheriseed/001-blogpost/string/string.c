/*
 * Copyright (c) 2022 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * **************************************************************************
 * This is for demonstration purposes only, shall not be used in production.
 * **************************************************************************
 *
 * This is an example implementation of simple string allocation.
 *
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_LENGTH 0x10

// Fixed-length string.
struct simple_string_t {
    char buffer[BUFFER_LENGTH + 1];
    int length;
};

// Allocate a new string and copy the characters from str_addr.
struct simple_string_t *new_string(intptr_t str_addr) {
    struct simple_string_t *new_str = calloc(1, sizeof(struct simple_string_t));
    new_str->length = strnlen((const char*)str_addr, BUFFER_LENGTH);
    memcpy(new_str->buffer, (const void *)str_addr, new_str->length);
    return new_str;
}

// Global variable.
char *str = "This is a string";

// Main function.
int main() {
    intptr_t str_ptr = (intptr_t)str;
    struct simple_string_t *string = new_string(str_ptr);

    printf("[string] \"%s\"\n", string->buffer);
    free(string);
    return 0;
}
