define i8 @foo(%__cheriseed_cap_t* nocapture readonly %0) {
  %2 = call i64 @__cheriseed_check_access(%__cheriseed_cap_t* %0, i64 1, i32 4, i64 0)
  %3 = inttoptr i64 %2 to i8*
  %4 = load i8, i8* %3, align 1
  ret i8 %4
}
