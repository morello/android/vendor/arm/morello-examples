/*
 * Copyright (c) 2022 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * **************************************************************************
 * This is for demonstration purposes only, shall not be used in production.
 * **************************************************************************
 */
char foo (char *bar) {
    return *bar;
}
