/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#ifdef __CHERI__
#include <cheriintrin.h>
#endif

// Default length of a compartment's memory range.
constexpr size_t kCompartmentMemoryRangeLength = 256 * 1024 * 1024;

constexpr size_t kCompartmentStackSize = 1024 * 1024;

// Environment variables propagated to the compartments.
constexpr const char* kCompartmentPropagatedEnv[] = {
  "PATH",
};

// Only keep the minimum permissions for compartment capabilities.
constexpr size_t kCompartmentDataPerms =
    CHERI_PERM_LOAD | CHERI_PERM_LOAD_CAP | ARM_CAP_PERMISSION_MUTABLE_LOAD |
    CHERI_PERM_STORE | CHERI_PERM_STORE_CAP | CHERI_PERM_STORE_LOCAL_CAP |
    CHERI_PERM_GLOBAL;
constexpr size_t kCompartmentExecPerms =
    CHERI_PERM_EXECUTE |
    CHERI_PERM_GLOBAL;
