/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "compartment_interface.h"

#ifdef __CHERI__
#include <cheriintrin.h>
#endif

#include "compartment_interface_impl.h"
#include "compartment_manager_asm.h"

uintcap_t CompartmentCall(CompartmentId id,
                          uintcap_t arg0, uintcap_t arg1, uintcap_t arg2,
                          uintcap_t arg3, uintcap_t arg4, uintcap_t arg5) {
  // CompartmentCallImpl() uses a capability function pointer to call CompartmentSwitch(), derive
  // one from PCC. A capability branch is needed anyway, because CompartmentSwitch() returns to the
  // caller using CLR.
  return CompartmentCallImpl(id, arg0, arg1, arg2, arg3, arg4, arg5,
                             cheri_address_set(cheri_pcc_get(),
                                               reinterpret_cast<uintptr_t>(&CompartmentSwitch)));
}
